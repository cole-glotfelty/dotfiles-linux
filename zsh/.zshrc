#!bin/sh
[ -f "/Users/coleglotfelty/.ghcup/env" ] && source "/Users/coleglotfelty/.ghcup/env" # ghcup-env

# Inspired by https://github.com/ChristianChiarulli/Machfiles/tree/master/zsh
[ -f "$HOME/.local/share/zap/zap.zsh" ] && source "$HOME/.local/share/zap/zap.zsh"

# history
HISTFILE=~/.zsh_history

# source
plug "$HOME/.config/zsh/aliases.zsh"
plug "$HOME/.config/zsh/exports.zsh"

# enable colors
autoload -U colors && colors

# auto/tab completion
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)

# plugins
plug "zsh-users/zsh-autosuggestions"
plug "zap-zsh/supercharge"
plug "zap-zsh/atmachine-prompt"
plug "zap-zsh/exa"
plug "zsh-users/zsh-syntax-highlighting"

# keybinds
bindkey '^ ' autosuggest-accept

# PyWal
(cat ~/.cache/wal/sequences &)
